export interface IHeroCharacter {
  heroName: string;
  picture: string;
}